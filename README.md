- 程序基于`libjpeg-9e`解码库完成AVI视频解码。
- `libjpeg_for_windows_release` 程序运行环境为Windows(x86)，通过读取本地AVI文件并完成解码工作，将图像/音频数据发送给电脑屏幕/声卡，实现视频播放。
- `libjpeg_for_esp32_release` 程序运行环境为NodeMCU32s，通过读取SD卡内AVI文件，在NodeMCU32s上完成解码工作以及视频播放。
- 具体说明参考：[https://blog.csdn.net/weixin_42258222/article/details/126446040](https://blog.csdn.net/weixin_42258222/article/details/126446040) 🚀

