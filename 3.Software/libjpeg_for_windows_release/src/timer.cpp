#include "timer.h"

HANDLE hTimerQueue = NULL;

/* 功能：定时完成后，调用该函数（每次触发时都会调用回调函数，哪怕前一个回调函数还没执行完）
   参数：
         lpParameter：传入指定参数指针
         TimerOrWaitFired：参数为 TRUE，则等待超时；参数为 FALSE，则表示已向等待事件发出信号，对于计时器回调，此参数始终为 TRUE
   返回值：
         无
*/
void CALLBACK TimerRoutine(PVOID lpParameter, BOOLEAN TimerOrWaitFired)
{
    if (NULL == lpParameter) {
        printf("lpParameter is NULL\n");
        //return;
    }
    else {
        *(int*)lpParameter = 1; // frameup
        //printf("frameup: %d\n", frameup);
    }
}



/*功能：创建一个定时器
参数：
        time: 定时时间为time毫秒, 回调函数为TimerRoutine
返回值：
        1：为error
        0：为success      
*/
int create_time(unsigned int time)
{
    /* 定时器相关 */
    HANDLE hTimer = NULL;  

    // 不创建队列，则与默认计时器队列相关联
    hTimerQueue = CreateTimerQueue();
    if (NULL == hTimerQueue) {
        OutputDebugString(_T("CreateTimerQueue Error"));
        return 1;
    }

    if (!CreateTimerQueueTimer(&hTimer, hTimerQueue, WAITORTIMERCALLBACK(TimerRoutine), (int *)&frameup, 0, time, NULL))
    {
        OutputDebugString(_T("CreateTimerQueueTimer Error"));
        return 1;
    }

    return 0;
}


/*功能：关闭定时器队列
参数：
        无
返回值：
        1：为error
        0：为success
*/
int delete_timer(void)
{
    if (!DeleteTimerQueue(hTimerQueue))
    {
        OutputDebugString(_T("DeleteTimerQueue Error"));
        return 1;
    }
    return 0;
}