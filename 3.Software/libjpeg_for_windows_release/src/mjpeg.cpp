#include "mjpeg.h"

using namespace std;

struct jpeg_decompress_struct *cinfo;
struct my_error_mgr *jerr;
u8* jpegbuf;			//jpeg数据缓存指针
u32 jbufsize;			//jpeg buf大小



u8* jmembuf;			//mjpeg解码的内存池
u32 jmempos;			//内存池指针

u16* outlinebuf;		//输出行缓存,≥图像宽度*2字节

int Windows_Width = 0;
int Windows_Height = 0;

SDL_Window* screen = NULL;
SDL_Renderer* render = NULL;
SDL_Texture* texture = NULL;
unsigned char* r = NULL;

typedef struct my_error_mgr* my_error_ptr;

METHODDEF(void) my_error_exit(j_common_ptr cinfo)
{
    my_error_ptr myerr = (my_error_ptr)cinfo->err;

    (*cinfo->err->output_message) (cinfo);

    longjmp(myerr->setjmp_buffer, 1);
}


METHODDEF(void) my_emit_message(j_common_ptr cinfo, int msg_level)
{
    my_error_ptr myerr = (my_error_ptr)cinfo->err;
    if (msg_level < 0)
    {
        printf("emit msg:%d\r\n", msg_level);
        longjmp(myerr->setjmp_buffer, 1);
    }
}


//解码一副JPEG图片
//buf:jpeg数据流数组
//bsize:数组大小
//返回值:0,成功
//    其他,错误
u8 mjpegdec_decode(u8* buf, u32 bsize)
{
    JSAMPARRAY buffer; /* Output row buffer */ // unsigned char ** buffer;
    if (bsize == 0) return 1;

    int row_stride;		/* physical row width in output buffer */

    cinfo->err = jpeg_std_error(&jerr->pub);
    jerr->pub.error_exit = my_error_exit;
    jerr->pub.emit_message = my_emit_message;

    if (setjmp(jerr->setjmp_buffer)) //错误处理
    {
        jpeg_abort_decompress(cinfo);
        jpeg_destroy_decompress(cinfo);
        return 2;
    }
    jpeg_create_decompress(cinfo);
    jpeg_mem_src(cinfo, buf, bsize);   // 测试正常
    jpeg_read_header(cinfo, TRUE);
   
    jpeg_start_decompress(cinfo);

#if __DEBUG__
    printf("image_width = %d\n", cinfo->image_width);
    printf("image_height = %d\n", cinfo->image_height);
    printf("num_components = %d\n", cinfo->num_components);

    printf("output_width = %d\n", cinfo->output_width);
    printf("output_components = %d\n", cinfo->output_components);
#endif


    // 准备一幅w*h的RGB图像数据
    shared_ptr<unsigned char> rgb(new unsigned char[Windows_Width * Windows_Height * 4]);  // 乘以4是因为像素格式已指定为ARGB888，单个像素点占4字节
    r = rgb.get();

    row_stride = cinfo->output_width * cinfo->output_components;

    // 计算buffer大小并申请相应空间
    buffer = (*cinfo->mem->alloc_sarray)
        ((j_common_ptr)cinfo, JPOOL_IMAGE, row_stride, 1);

    int j = 0;
    int lineR = 0;   // 每一行R分量的起始位置

    while (cinfo->output_scanline < cinfo->output_height)
    {
        int i = 0;
        jpeg_read_scanlines(cinfo, buffer, 1);

        
        // 为上述图像数据赋值
        for (int k = 0; k < Windows_Width * 4; k += 4)
        {
            r[lineR + k] = buffer[0][i + 2];                                      // B
            r[lineR + k + 1] = buffer[0][i + 1];                                  // G
            r[lineR + k + 2] = buffer[0][i];                                      // R
            r[lineR + k + 3] = 0;                                                 // A
            i += 3;
        }

        j++;
        lineR = j * Windows_Width * 4;
    }
    
    // 执行显示操作
    Image_Display();
    

    jpeg_finish_decompress(cinfo);
    jpeg_destroy_decompress(cinfo);
    return 0;

}

int put_scanline_someplace(JSAMPROW buffer, int row_stride)
{
    int i = 0;
    for (; i < row_stride; i++) {
        printf("0x%x ", buffer[i]);
    }

    printf("\nend\n");
    return 0;
}


// windows 显示初始化
int Image_Init(void)
{
    // 1. 初始化SDL库， 成功返回0, 失败返回非0值
    if (SDL_Init(SDL_INIT_VIDEO))
    {
        cout << SDL_GetError() << endl;
        return -1;
    }

    // 2. 创建SDL窗口
    screen = SDL_CreateWindow("libjpeg_for_windows",                   // 窗口标题
        SDL_WINDOWPOS_CENTERED,                                         // 窗口位置
        SDL_WINDOWPOS_CENTERED,
        Windows_Width, Windows_Height,                                                           // 窗口宽高
        //640,360,
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE                          // 窗口属性，指定使用OpenGL, 并且可调整大小
    );
    if (!screen)
    {
        cout << SDL_GetError() << endl;
        return -2;
    }

    // 3. 创建渲染器
    render = SDL_CreateRenderer(screen,                            // 指定渲染到哪个窗口
        -1,                                                             // 指定渲染器驱动，默认传-1
        SDL_RENDERER_ACCELERATED                                        // 指定渲染模式，这里采用硬件加速模式
    );
    if (!render)
    {
        cout << SDL_GetError() << endl;
        return -3;
    }

    // 4. 在渲染器当中创建一个材质
    texture = SDL_CreateTexture(render,                            // 指定在哪个渲染器当中创建
        SDL_PIXELFORMAT_ARGB8888,                                       // 指定当前材质的像素格式
        SDL_TEXTUREACCESS_STREAMING,                                    // 设定当前材质可修改
        Windows_Width, Windows_Height                                   // 指定材质宽高
        //640,360
    );
    if (!texture)
    {
        cout << SDL_GetError() << endl;
        return -4;
    }

    return 0;
}

// windows 显示操作
int Image_Display(void)
{
    // 5. 将内存中的RGB数据写入材质
    if (SDL_UpdateTexture(texture, NULL, r, Windows_Width * 4))
    {
        cout << SDL_GetError() << endl;
        return -5;
    }


    // 6. 清理渲染区(清理屏幕)
    if (SDL_RenderClear(render))
    {
        cout << SDL_GetError() << endl;
        return -6;
    }

    // 设定渲染的目标区域
    SDL_Rect destRect;
    destRect.x = 0;
    destRect.y = 0;
    destRect.w = Windows_Width;
    destRect.h = Windows_Height;

    // 7. 复制材质到渲染器对象
    if (SDL_RenderCopy(render, texture, NULL, &destRect))
    {
        cout << SDL_GetError() << endl;
        return -7;
    }

    // 8. 执行渲染操作
    SDL_RenderPresent(render);

    return 0;
}

//mjpeg 解码初始化
//offx,offy:x,y方向的偏移
//返回值:0,成功;
//       1,失败
u8 mjpegdec_init(void)
{
    cinfo = (struct jpeg_decompress_struct *)malloc(sizeof(struct jpeg_decompress_struct));
    jerr = (struct my_error_mgr *)malloc(sizeof(struct my_error_mgr));

    if (cinfo == NULL || jerr == NULL)
    {
        mjpegdec_free();
        return 1;
    }

    return 0;
}

//mjpeg结束,释放内存
void mjpegdec_free(void)
{
    free(cinfo);
    free(jerr);
}