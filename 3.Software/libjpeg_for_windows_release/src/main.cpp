#include "mjpeg.h"
#include "videoplayer.h"

using namespace std;


// SDL 库里面定义了一个宏 main, 这里给取消掉，否则会与main 函数名冲突，导致编译错误
#undef main

int main(int argc, const char *argv[])
{
    video_play_mjpeg((u8 *)".\\video\\video.avi");
    return 0;
}
