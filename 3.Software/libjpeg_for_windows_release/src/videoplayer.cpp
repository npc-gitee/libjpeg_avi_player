#include "videoplayer.h"

u8* saibuf; 			    // 音频缓冲帧,5K
u16 frame = 0;              // 记录当前所处帧的位置
vu32 frameup;               // 控制帧率标志位，用来控制帧数

SDL_Event ev;

//播放一个mjpeg文件
//pname:文件名
//返回值：空
u8 video_play_mjpeg(u8* pname)
{
	u8* framebuf; // 视频解码buf
	u8* pbuf;      // buf指针
	FILE* favi = NULL;              // 文件句柄
	u8 res = 0;
	unsigned int read_count = 0;    // 读取的数据字节数
	u16 offset = 0;


	saibuf = (u8 *)malloc(AVI_AUDIO_BUF_SIZE);	//申请音频内存
	framebuf = (u8 *)malloc(AVI_VIDEO_BUF_SIZE);	//申请视频buf

	if (!saibuf) {
		printf("memory error!\r\n");
		res = 0xFF;
	}
	memset(saibuf, 0, AVI_AUDIO_BUF_SIZE);


	while (res == 0) {
		favi = fopen((const char *)pname, "rb");

		if (favi != NULL) {
			printf("\nopen avi ok\n");
			pbuf = framebuf;
			unsigned int count = 0;
			read_count = fread(pbuf, 1, AVI_VIDEO_BUF_SIZE, favi); // 开始读取，读取60KB
			
			if (read_count == 0) {
				printf("fread EOF: %d\n", read_count);
				break;
			}
			printf("read_count: %d\n", read_count);

			// 开始avi解析
			res = avi_init(pbuf, AVI_VIDEO_BUF_SIZE);
			if (res) {
				printf("avi err:%d\r\n", res);
				break;
			}
			video_info_show(&avix);

			//按照帧率设定中断，windows环境下采用多线程
			create_time(50); // 帧率20


			offset = avi_search_id(pbuf, AVI_VIDEO_BUF_SIZE, (u8*)"movi"); // 返回"movi"中'm'的偏移位置
			
			// 调试
#if __DEBUG__
			printf("offset:%d\n", offset);
#endif
			
			avi_get_streaminfo(pbuf + offset + 4);			//获取流信息
			
			fseek(favi, offset+12, SEEK_SET); // 跳过标志ID,读地址偏移到流数据开始处
			res = mjpegdec_init();//JPG解码初始化

			// 音频硬件初始化
			if (avix.SampleRate) { 
				/* 设置声卡参数 */
				//44100到时候需要根据歌曲的实际采样率来填写，16位深度，双声道
				Set_WIN_Params(NULL, avix.SampleRate, 16, avix.Channels);
			}

			// 定义图像的宽高
			Windows_Width = avix.Width;
			Windows_Height = avix.Height;

			// 显示初始化
			if (Image_Init() != 0) {
				printf("Image windows Init error\n");
				return -1;
			}

			while (1) {  // 播放循环
				if (avix.StreamID == AVI_VIDS_FLAG) {  // 视频流
					pbuf = framebuf;
					fread(pbuf, 1, avix.StreamSize + 8, favi); //读入整帧+下一数据流ID信息
					res = mjpegdec_decode(pbuf, avix.StreamSize);
					if (res) {
						printf("decode error!\r\n");
					}
					
					while (frameup == 0);   //等待时间到达(定时中断里面设置为1)
					frameup = 0;			//标志清零
					frame++;
				}
				else {   // 音频流
					fread(saibuf, 1, avix.StreamSize + 8, favi);
					pbuf = saibuf;
					WIN_Play_Samples(saibuf, avix.StreamSize);
				}

				if (SDL_PollEvent(&ev) != 0) {   // 检测主动退出事件（点击"x"）
					if (ev.type == SDL_QUIT) {
						SDL_DestroyWindow(screen);
						res = 0xFF;  // 退出显示窗口标志
						break;
					}               
				}

				if (avi_get_streaminfo(pbuf + avix.StreamSize))//读取下一帧 流标志
				{
					printf("frame error\r\n");
					res = 1;
					break;
				}
			}
		}
		else {
			printf("File does not exist or fopen error!\n");
			return -1;
		}
	}

	free(saibuf);
	free(framebuf);
	
	WIN_Audio_close(); // 关闭声卡设备
	delete_timer();    // 删除定时器
	mjpegdec_free();   // 释放内存资源
	fclose(favi);      // 关闭文件

	printf("\nvideo_play_mjpeg: bye-bye\n");

	return res;
}

// 显示当前视频文件的相关信息
// aviinfo;avi控制结构体
void video_info_show(AVI_INFO* aviinfo)
{
	u8* buf;
	buf = (u8*)malloc(100); // 申请100字节内存
	sprintf((char*)buf, "声道数: %d, 采样率:%d", aviinfo->Channels, aviinfo->SampleRate);

	printf("%s\n", buf);
	sprintf((char*)buf, "帧率: %d", 1000/(aviinfo->SecPerFrame/1000));  // 1000ms/(us/1000=ms)
	printf("%s\n", buf);
	free(buf);
}


// 得到stream流信息
// buf: 流开始地址(必须是01wb/00wb/01dc/00dc开头)
AVISTATUS avi_get_streaminfo(u8* buf)
{
	avix.StreamID = Loc_MAKEWORD(buf + 2);      // 得到流类型
	avix.StreamSize = Loc_MAKEDWORD(buf + 4);   // 得到流大小

	if (avix.StreamSize % 2) avix.StreamSize++; // 奇数加1（avix.StreamSize 必须是偶数）
	if (avix.StreamID == AVI_VIDS_FLAG || avix.StreamID == AVI_AUDS_FLAG) return AVI_OK;
	return AVI_STREAM_ERR;
}
