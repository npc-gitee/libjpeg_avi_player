#ifndef __MY_TIMER__H
#define __MY_TIMER__H

#include <iostream>
#include <windows.h>
#include <tchar.h>
#include "videoplayer.h"


int create_time(unsigned int time);
int delete_timer(void);

#endif