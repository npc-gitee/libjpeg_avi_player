#ifndef __MJPEG__
#define __MJPEG__

#include <stdio.h>
#include "jpeglib.h"
#include <setjmp.h>

#include <iostream>
#include "SDL.h"
#include "common.h"

#pragma comment(lib, "SDL2.lib")

#define __DEBUG__ 0

//简单快速的内存分配,以提高速度
#define MJPEG_MAX_MALLOC_SIZE 		38*1024			//最大可以分配38K字节


struct my_error_mgr {
    struct jpeg_error_mgr pub;	/* "public" fields */

    jmp_buf setjmp_buffer;	/* for return to caller */
};


// 变量声明
extern JSAMPLE* image_buffer;	/* Points to large array of R,G,B-order data */
extern int image_height;	/* Number of rows in image */
extern int image_width;		/* Number of columns in image */

extern int Windows_Width;       // 图像宽度
extern int Windows_Height;      // 图像高度

extern SDL_Window* screen;
extern SDL_Renderer* render;
extern SDL_Texture* texture;
extern unsigned char* r;



// 函数声明
int put_scanline_someplace(JSAMPROW buffer, int row_stride);
int Image_Init(void);
int Image_Display(void);
u8 mjpegdec_init(void);
void mjpegdec_free(void);
u8 mjpegdec_decode(u8* buf, u32 bsize);


GLOBAL(void) write_JPEG_file(char* filename, int quality);
GLOBAL(int) read_JPEG_file(char* filename);



#endif