#ifndef __VEDIOPLAYER__
#define __VEDIOPLAYER__

#include <stdio.h>
#include <stdlib.h>
#include "mjpeg.h"
#include "avi.h"
#include "common.h"
#include "timer.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "wave_out.h"

#ifdef __cplusplus
}
#endif



#define AVI_AUDIO_BUF_SIZE    1024*5		//定义avi解码时,音频buf大小.
#define AVI_VIDEO_BUF_SIZE    1024*60		//定义avi解码时,视频buf大小.

#define	 Loc_MAKEWORD(ptr)	(u16)(((u16)*((u8*)(ptr))<<8)|(u16)*(u8*)((ptr)+1))
#define  Loc_MAKEDWORD(ptr)	(u32)(((u16)*(u8*)(ptr)|(((u16)*(u8*)(ptr+1))<<8)|(((u16)*(u8*)(ptr+2))<<16)|(((u16)*(u8*)(ptr+3))<<24))) 

extern vu32 frameup;

u8 video_play_mjpeg(u8* pname);
void video_info_show(AVI_INFO* aviinfo);
AVISTATUS avi_get_streaminfo(u8* buf);

#endif