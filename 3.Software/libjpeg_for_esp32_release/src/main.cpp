/*
 * Connect the SD card to the following pins:
 *
 * SD Card | ESP32
 *    D2       12
 *    D3       13
 *    CMD      15
 *    VSS      GND
 *    VDD      3.3V
 *    CLK      14
 *    VSS      GND
 *    D0       2  (add 1K pull up after flashing)
 *    D1       4
 */

#include "FS.h"
#include "SD.h"
#include "SD_MMC.h"
#include <SPI.h>
#include "TFT_eSPI.h"
#include <stdio.h>
#include <setjmp.h>
#include <TaskScheduler.h>

#define boolean libjpeg_boolean
#include "jpeglib.h"
#undef boolean

#include "videoplayer.h"
#include "lcd_layout.h"
#include "get_time_display.h"
#include "serial_interactive.h"
#include "read_ziku.h"

bool SD_INIT_OK = FALSE;
TFT_eSPI tft = TFT_eSPI(240.240);
TaskHandle_t TASK_HandleTwo = NULL;
struct videoplayer_parm global_videoplayer_parm;
SemaphoreHandle_t mutex_v;  // 互斥量
char jh_buffer[64];

void setup() {

  Serial.begin(115200);
  
  // 创建互斥量
  mutex_v = xSemaphoreCreateMutex();

  Serial.println("请插入内存卡");
  delay(6000);

  /* test SD_MMC 4-bit Mode */
  if (!SD_MMC.begin()) {
    Serial.println("Card Mount Failed");
    return;
  }
  SD_INIT_OK = TRUE;

  global_videoplayer_parm.sdmmc = (void *)&SD_MMC;

  // 初始化LCD，完成初步显示
  lcd_layout_init();

  // 连接wifi
  get_wifi();

  xTaskCreate( get_time_display,                             /* 任务函数 */
              "Tasktwo",                                     /* 任务名 */
              4*1024,                                        /* 任务栈大小，根据需要自行设置*/
              NULL,                                          /* 参数，入参为空 */
              1,                                             /* 优先级 */
              &TASK_HandleTwo);                              /* 任务句柄 */


  //SD_MMC.end(); // 取消SD挂载
  help_info();
}

void loop() {
  if(TRUE == SD_INIT_OK){
    memset(jh_buffer, 0, 64);
    mySerial_Recv(jh_buffer);
    Serial.printf("Input Command Is [ %s ]\n", jh_buffer);
    if(strncmp(jh_buffer, "ls", 2) == 0){
        commd_ls(SD_MMC, "/", 0);
    }
    else if(strncmp(jh_buffer, "play ", 5) == 0){
        commd_play(SD_MMC, jh_buffer);
    }
  }
}
