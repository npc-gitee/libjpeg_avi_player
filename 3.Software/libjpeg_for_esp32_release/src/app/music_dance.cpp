#include "music_dance.h"

int pix_length[] = {1, 3, 5, 6, 1, 3, 5, 6};


/* 作用: 音符显示初始化
 * 参数: 音符显示宽度
 * 返回值: 无
*/
void music_dance_init(int width)
{
    if(width != 5 && width != 3){
        printf("[ music_dance_init ]: please input 3 or 5\n");
        return;
    }
    
    if(5 == width){
        tft.fillCircle(204, 162, 2, TFT_WHITE);   // 画右边圆形（填充）    最小——>之后变大
        tft.fillRect(202, 163, 5, 2, TFT_WHITE);   // 画矩形（填充）
        tft.fillCircle(204, 165, 2, TFT_WHITE);   // 画右边圆形（填充）

        tft.fillCircle(213, 156, 2, TFT_WHITE);   // 画右边圆形（填充）    最大——>之后变小
        tft.fillRect(211, 157, 5, 14, TFT_WHITE);   // 画矩形（填充）
        tft.fillCircle(213, 171, 2, TFT_WHITE);   // 画右边圆形（填充）

        tft.fillCircle(222, 159, 2, TFT_WHITE);   // 画右边圆形（填充）    中等——>之后变大
        tft.fillRect(220, 160, 5, 8, TFT_WHITE);   // 画矩形（填充）
        tft.fillCircle(222, 168, 2, TFT_WHITE);   // 画右边圆形（填充）
    }
    else{
        tft.fillCircle(204, 162, 1, TFT_WHITE);   // 画右边圆形（填充）    最小——>之后变大
        tft.fillRect(203, 163, 3, 2, TFT_WHITE);   // 画矩形（填充）
        tft.fillCircle(204, 165, 1, TFT_WHITE);   // 画右边圆形（填充）

        tft.fillCircle(213, 156, 1, TFT_WHITE);   // 画右边圆形（填充）    最大——>之后变小
        tft.fillRect(212, 157, 3, 14, TFT_WHITE);   // 画矩形（填充）
        tft.fillCircle(213, 171, 1, TFT_WHITE);   // 画右边圆形（填充）

        tft.fillCircle(222, 159, 1, TFT_WHITE);   // 画右边圆形（填充）    中等——>之后变大
        tft.fillRect(221, 160, 3, 8, TFT_WHITE);   // 画矩形（填充）
        tft.fillCircle(222, 168, 1, TFT_WHITE);   // 画右边圆形（填充）
    }
}


/* 作用: 音符动态显示
 * 参数: 音符的宽度，5 pix or 3 pix
 * 返回值: 无 
*/
void music_dance(void *para)
{
    int index_left = 0;
    int index_middle = 4;
    int index_right = 2;
    int width = *(int *)para;
    int radius = 0;

#if __DEBUG__
    printf("[M][music_dance.cpp] music_dance(): Task-music dance\r\n");
#endif

    if(width != 5 && width != 3){
        printf("[E][music_dance.cpp] music_dance(): please input 3 or 5\r\n");
        return;
    }

    if(5 == width){
        radius = 2;
    }
    else{
        radius = 1;
    }

    while(1){
        if(8 == index_left){
            index_left = 0;
        }

        if(8 == index_middle){
            index_middle = 0;
        }

        if(8 == index_right){
            index_right = 0;
        }

        xSemaphoreTake(mutex_v, portMAX_DELAY);  // 申请互斥锁，若没有资源，则无限等待资源

        // 左边的
        if(index_left >= 4){ // 减小
            tft.fillRect(204-radius, 156-radius, width, pix_length[index_left]+(radius+1), TFT_BLACK);
            tft.fillCircle(204, 156+pix_length[index_left], radius, TFT_WHITE);

            tft.fillRect(204-radius, 171-pix_length[index_left], width, pix_length[index_left]+(radius+1), TFT_BLACK);
            tft.fillCircle(204, 171-pix_length[index_left], radius, TFT_WHITE);
        }
        else{ // 增加
            tft.fillRect(204-radius, 163-pix_length[index_left], width, pix_length[index_left], TFT_WHITE);
            tft.fillCircle(204, 162-pix_length[index_left], radius, TFT_WHITE);

            tft.fillRect(204-radius, 164, width, pix_length[index_left], TFT_WHITE);
            tft.fillCircle(204, 164+pix_length[index_left], radius, TFT_WHITE);
        }

        // 中间的
        if(index_middle >= 4){ // 减小
            tft.fillRect(213-radius, 156-radius, width, pix_length[index_middle]+(radius+1), TFT_BLACK);
            tft.fillCircle(213, 156+pix_length[index_middle], radius, TFT_WHITE);

            tft.fillRect(213-radius, 171-pix_length[index_middle], width, pix_length[index_middle]+(radius+1), TFT_BLACK);
            tft.fillCircle(213, 171-pix_length[index_middle], radius, TFT_WHITE);
        }
        else{ // 增加
            tft.fillRect(213-radius, 163-pix_length[index_middle], width, pix_length[index_middle], TFT_WHITE);
            tft.fillCircle(213, 162-pix_length[index_middle], radius, TFT_WHITE);

            tft.fillRect(213-radius, 164, width, pix_length[index_middle], TFT_WHITE);
            tft.fillCircle(213, 164+pix_length[index_middle], radius, TFT_WHITE);
        }

        // 右边的
        if(index_right >= 4){ // 减小
            tft.fillRect(222-radius, 156-radius, width, pix_length[index_right]+(radius+1), TFT_BLACK);
            tft.fillCircle(222, 156+pix_length[index_right], radius, TFT_WHITE);

            tft.fillRect(222-radius, 171-pix_length[index_right], width, pix_length[index_right]+(radius+1), TFT_BLACK);
            tft.fillCircle(222, 171-pix_length[index_right], radius, TFT_WHITE);
        }
        else{ // 增加
            tft.fillRect(222-radius, 163-pix_length[index_right], width, pix_length[index_right], TFT_WHITE);
            tft.fillCircle(222, 162-pix_length[index_right], radius, TFT_WHITE);

            tft.fillRect(222-radius, 164, width, pix_length[index_right], TFT_WHITE);
            tft.fillCircle(222, 164+pix_length[index_right], radius, TFT_WHITE);
        }

        xSemaphoreGive(mutex_v);  // 释放互斥锁

        index_left++;
        index_middle++;
        index_right++;

        vTaskDelay(100);          // 定时+防止触发watch dog
    }

}
