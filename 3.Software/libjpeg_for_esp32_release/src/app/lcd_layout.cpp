#include "lcd_layout.h"

const char utf8_time_sym[4] = "-";
const char utf8_date[16] = "年月日周";

/* 作用: LCD 初始化以及界面布局显示初始化
 * 参数: 无
 * 返回值: 无
*/
void lcd_layout_init(void)
{
    unsigned char *zimo_buffer = NULL;
    tft.init(); //LCD初始化
    tft.fillScreen(TFT_WHITE); //屏幕颜色
    tft.setRotation(0); //不旋转显示角度

    tft.fillRect(0, 135, 240, 2, TFT_BLACK);   // 画矩形（填充）/分割线

    tft.fillCircle(24, 163, 20, TFT_BLACK);    // 画左边圆形（填充）
    tft.fillCircle(167, 163, 20, TFT_BLACK);   // 画中间圆形（填充）
    tft.fillRect(24, 143, 145, 41, TFT_BLACK); // 画矩形（填充）
    tft.fillCircle(213, 163, 20, TFT_BLACK);   // 画右边圆形（填充）
  
    tft.fillCircle(24, 212, 20, TFT_BLACK);    // 画左边圆形（填充）
    tft.fillCircle(213, 212, 20, TFT_BLACK);   // 画右边圆形（填充）
    tft.fillRect(24, 192, 187, 41, TFT_BLACK); // 画矩形（填充）

    lcd_display_love(); // 显示“爱心”
    lcd_display_pig();  // 显示“小猪”

    zimo_buffer = (unsigned char *)malloc(128);
    if(NULL == zimo_buffer){
        printf("[E][lcd_layout.cpp] lcd_layout_init(): malloc failed to apply for memory\r\n");
        return;
    }

    read_single_ziku((char *)utf8_time_sym, zimo_buffer);
    tft.drawBitmap(127, 154, zimo_buffer, 24, 20, TFT_WHITE);             // "-"
    
    tft.drawBitmap(148, 152, maohao, 16, 20, TFT_WHITE);                  // ":"

    read_single_ziku((char *)utf8_date, zimo_buffer);                     // "年"
    tft.drawBitmap(67, 202, zimo_buffer, 24, 20, TFT_WHITE);

    read_single_ziku((char *)utf8_date+3, zimo_buffer);                   // "月"
    tft.drawBitmap(115, 203, zimo_buffer, 24, 20, TFT_WHITE);

    read_single_ziku((char *)utf8_date+6, zimo_buffer);                   // "日"
    tft.drawBitmap(159, 203, zimo_buffer, 24, 20, TFT_WHITE);

    read_single_ziku((char *)utf8_date+9, zimo_buffer);                   // "周"
    tft.drawBitmap(185, 203, zimo_buffer, 24, 20, TFT_WHITE);

    free(zimo_buffer);
}



