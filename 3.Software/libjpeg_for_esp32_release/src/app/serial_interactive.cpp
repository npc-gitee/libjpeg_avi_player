#include "serial_interactive.h"

/* 作用: 打印帮助信息
 * 参数: 无
 * 返回值: 无
*/
void help_info(void)
{
  Serial.println("************************************");
  Serial.println("***  ls: view file information   ***");
  Serial.println("***  play <filename>: play video ***");
  Serial.println("************************************");
}

/* 作用: 获取串口字符串数据
 * 参数: 
 *      buffer: 数据保存位置
 * 返回值: 无
*/
void mySerial_Recv(char *buffer)
{
  Serial.println("[plz input command]");
  Serial.printf(">");
  int i = 0;
  while(1){
    if(Serial.available()){
      buffer[i] = Serial.read();
      if(buffer[i] == '\n'){
        break;
      }
      else{
        i++;
      }
    }
  }
  buffer[i] = '\0';
}

/* 作用: 获取指定目录下的文件
 * 参数: 
 *      fs: 句柄
 *      dirname: 指定查询目录
 *      levels: 目录查询层级，如果为0，则只查看当前层级的文件
 * 返回值: 无
*/
void commd_ls(fs::FS &fs, const char * dirname, uint8_t levels)
{
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                commd_ls(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }

    root.close();
}

/* 作用: 播放avi音频
 * 参数: 
 *       fs: 句柄
 *       buf: 命令字符串存储首地址
 * 返回值: 无
*/
void commd_play(fs::FS &fs, char *buf)
{
  global_videoplayer_parm.file_name = buf+5;

  if(!fs.exists((const char *)buf+5)){
    printf("[E][serial_interactive.cpp] commd_play(): File does not exist\r\n");
    return;
  }

  // 通过后缀名判断是否为avi文件
  if(is_avi_file(buf, strlen(buf)) == 1){
    return;
  }

  // 显示文件名
  EtoC_index(fs, buf+6); // 偏移一个字符，因为输入文件名带'/'的符号

  // 播放avi视频
  video_play_mjpeg((void*)&global_videoplayer_parm);
}


/* 作用: 判断播放文件是否为avi文件
 * 参数: 
 *      buf: 字符存储地址
 *      str_len: 字符串长度
 * 返回值: 0为avi文件，1为其它文件
*/
char is_avi_file(char *buf, int str_len)
{
  if(strncmp(buf+str_len-3, "avi", 3) == 0){
#if __DEBUG__
    printf("[M][serial_interactive.cpp] is_avi_file(): is avi file\r\n");
#endif
    return 0;
  }
  else{
    printf("[E][serial_interactive.cpp] is_avi_file(): is not avi file\r\n");
    return 1;
  }
}


/* 作用: 将字母形式的文件名转为中文/英文形式，并显示
 * 参数: 
 *      fs: 句柄
 *      buf: 字符存储首地址
 * 返回值: 无
*/
void EtoC_index(fs::FS &fs, char *buf)
{
  char read_buf[64] = {0};
  unsigned char *zimo_buffer;
  int str_len = strlen(buf);              // 不计入换行符
  int i = 0;
  int pix_acc = 43;
  int total_pix_width = 0;
  char find_flag = 0;
  File file = fs.open("/EtoC_index.txt");

#if __DEBUG__
  printf("str_len: %d\r\n", str_len);
  printf("%s\r\n", buf);
#endif

  while(file.available()){

    file.readBytesUntil('-', read_buf, 64); // 读到这个字符的前一个，下次读从这个字符的下一个开始读，就是跳过这个字符

    if(strncmp(buf, read_buf, str_len) == 0){
      find_flag = 1;
      tft.fillRect(43, 154, 80, 23, TFT_BLACK);  // 清屏

      zimo_buffer = (unsigned char *)malloc(128);
      if(NULL == zimo_buffer){
        printf("[E][lcd_layout.cpp] lcd_layout_init(): malloc failed to apply for memory\r\n");
        return;
      }

      file.readBytesUntil('\n', read_buf, 64); // 回车

      // 统计显示字符所需要的像素宽度
      total_pix_width = number_of_words(read_buf);
#if __DEBUG__
      printf("[M][serial_interactive.cpp] EtoC_index: total pix width %d\r\n", total_pix_width);
#endif

      while(read_buf[i] != '.'){
        if(read_buf[i] < 0x7F){ // 英文
          read_single_ziku(read_buf+i, zimo_buffer);
          tft.drawBitmap(pix_acc, 154, zimo_buffer, 24, 20, TFT_ORANGE);
          i++;
          pix_acc += 12;
        }
        else{ // 中文
          read_single_ziku(read_buf+i, zimo_buffer);                 
          tft.drawBitmap(pix_acc, 154, zimo_buffer, 24, 20, TFT_ORANGE);
          i+=3;
          pix_acc += 20;
        }
        
        if((pix_acc >= 103 ) && (total_pix_width > 80)){  // 文件名太长，最后空间来显示"..."
          read_buf[0] = '.';
          read_single_ziku(read_buf, zimo_buffer);                 
          tft.drawBitmap(105, 158, zimo_buffer, 24, 20, TFT_ORANGE);               
          tft.drawBitmap(112, 158, zimo_buffer, 24, 20, TFT_ORANGE);
          tft.drawBitmap(119, 158, zimo_buffer, 24, 20, TFT_ORANGE);
          break;
        }
      }
      free(zimo_buffer);
      break;
    }
    else{
      file.readBytesUntil('\n', read_buf, 64); // 读到下一行
    }
  }

  if(0 == find_flag){
    printf("[E][serial_interactive.cpp] EtoC_index: not found in EtoC_index.txt\r\n");
  }

}


/* 作用: 统计显示全部字符所需要的像素宽度
 * 参数: 字符存储地址
 * 返回值: 显示全部字符所需要的像素宽度
*/
int number_of_words(char *buf)
{
  int i = 0;
  int pix_acc = 0;
  while(buf[i] != '.'){
    if(buf[i] < 0x7F){ // 英文
      pix_acc+=12;
      i++;
    }
    else{  // 中文
      pix_acc+=20;
      i+=3;
    }
  }
  return pix_acc;
}