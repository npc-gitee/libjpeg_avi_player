#include "videoplayer.h"

vu32 frameup;                         // 控制帧率标志位，用来控制帧数
Ticker T_Frame;                       // 定时器对象
TaskHandle_t TASK_HandleOne = NULL;   // 跳跃音符句柄
int width = 0;                        // 跳跃音符宽度


/* 作用: 播放一个mjpeg文件
 * 参数: videoplayer_parm 结构体指针
 * 返回值: 空
*/
void video_play_mjpeg(void *parm)
{
    u8* framebuf;            // 指向视频/音频帧缓冲区
	//u8* pbuf;              // buf指针
    File file;
    u8 res = 0;              // 返回值
	u16 offset = 0;
    u16 frame_count = 0;     // 保存帧数，计算视频时长
    u32 totsec = 0;          // 记录播放剩余时长
    u8 local_frameRate = 0;  // 视频图像帧率
 
    struct videoplayer_parm * myparm = (struct videoplayer_parm *)parm;
    fs::FS &fs = *(fs::FS *)myparm->sdmmc;
    const char* pname = myparm->file_name;

    framebuf = (u8 *)malloc(AVI_FILE_HEAD_SIZE);	//申请60KB 用于分析AVI头文件
    
    if (!framebuf) {
		printf("[E][videoplayer.cpp] video_play_mjpeg(): malloc failed to applyed for memory [1]\r\n");
		goto end1;
	}

    memset(framebuf, 0, AVI_VIDEO_BUF_SIZE);
    
    file = fs.open(pname);
    if (file) {
#if __DEBUG__
        printf("[M][videoplayer.cpp] video_play_mjpeg(): open avi file ok\r\n");
#endif  

        file.read(framebuf, AVI_VIDEO_BUF_SIZE);  // 读取 60KB 数据

        // 开始 AVI 解析
        res = avi_init(framebuf, AVI_VIDEO_BUF_SIZE);   
        if (res) {
			printf("[E][videoplayer.cpp] video_play_mjpeg(): avi err-%d\r\n", res);
            goto end2;
		}

        totsec = video_cal_time(&avix);                   // 计算视频时长
        local_frameRate = 1000/(avix.SecPerFrame/1000);   // 计算视频帧率

        // 串口打印信息
        video_info_show(&avix);

        // LCD 显示视频时长
        if(lcd_display_videotime(totsec) == -1){
            goto end2;
        }

        offset = avi_search_id(framebuf, AVI_VIDEO_BUF_SIZE, (u8*)"movi"); // 返回"movi"中'm'的偏移位置

        if(avi_get_streaminfo(framebuf + offset + 4)){	//获取流信息
            printf("[E][videoplayer.cpp] video_play_mjpeg(): frame error\r\n");
			goto end2;
        }

        file.seek(offset+12);  // 跳过标志ID,读地址偏移到流数据开始处

        free(framebuf);
        framebuf = (u8 *)malloc(AVI_VIDEO_BUF_SIZE);
        if (!framebuf) {
		    printf("[E][videoplayer.cpp] video_play_mjpeg(): malloc failed to applyed for memory [2]\r\n");
		    goto end2;
	    }

        res = mjpegdec_init(avix.Width, avix.Height); //JPG解码初始化
        if(-1 == res){
            goto end2;
        }

        // 定义图像的宽高
		Windows_Width = avix.Width;
		Windows_Height = avix.Height;
            
        // 设置声卡参数
		if (22050 == avix.SampleRate) {
            //22050采样率,16位深度，双声道
            csound_audioInit((int)avix.SampleRate, 16);
        }
        else{
            printf("[E][videoplayer.cpp] video_play_mjpeg(): Not supported except 22050 sampling rate\r\n");
		    goto end2;
        }

        // 跳动音符初始化
        width = 5;
        music_dance_init(width);

        xTaskCreate(music_dance, "TaskOne", 2*1024, &width, 1, &TASK_HandleOne);

        // 定时，设置标志位，从而控制帧率(avix.SecPerFrame/1000)
        T_Frame.attach_ms((avix.SecPerFrame/1000), TimerRoutine, 0);

        while(1){  // 播放循环
            if (avix.StreamID == AVI_VIDS_FLAG) {  // 视频流
                //pbuf = framebuf;
                file.read(framebuf, avix.StreamSize + 8); //读入整帧+下一数据流ID信息
#if __DEBUG__
                printf("[M][videoplayer.cpp] video_play_mjpeg(): Video: StreamID = 0x%x  StreamSize = %ld\n", avix.StreamID, avix.StreamSize);
#endif

                res = mjpegdec_decode(framebuf, avix.StreamSize); // 完成解码+显示
                if (res) {
					printf("[E][videoplayer.cpp] video_play_mjpeg(): decode error-%d\r\n", res);
                    goto end3;
				}

                while (frameup == 0);   //等待时间到达(定时中断里面设置为1)
                frameup = 0;			//标志清零
				frame_count++;
                if(frame_count == local_frameRate){
                    frame_count = 0;
                    totsec--;
                    res = lcd_display_videotime(totsec);
                    if(-1 == res){
                        goto end3;
                    }
                }
            }
            else{
                file.read(framebuf, avix.StreamSize + 8); //读入整帧+下一数据流ID信息
#if __DEBUG__
                printf("[M][videoplayer.cpp] video_play_mjpeg(): Audio: StreamID = 0x%x  StreamSize = %ld\r\n", avix.StreamID, avix.StreamSize);
#endif	
                play_wav(framebuf, avix.StreamSize); // 数据转换+发送给DAC   
            }

            if (avi_get_streaminfo(framebuf + avix.StreamSize))//读取下一帧 流标志
			{
				printf("[E][videoplayer.cpp] video_play_mjpeg(): frame error\r\n");
				goto end3;
			}
        }
    }
    else{
        printf("[E][videoplayer.cpp] video_play_mjpeg(): File does not exist or fopen error!\r\n");
        goto end1;
    }
    
end3:
    vTaskDelay(500);              // 数字变化不会很快
    lcd_display_videotime(0);     // 当前计数帧数没有达到帧率，则视频停止后，显示剩余时间为1,所以这里最后统一显示成0    
    vTaskDelete(TASK_HandleOne);
end2:
    file.close();
end1:
    free(framebuf);
    printf("\nvideo_play_mjpeg: bye-bye\n");
}

/* 作用: 串口显示当前视频文件的相关信息
 * 参数: avi信息
 * 返回值: 无
*/
void video_info_show(AVI_INFO* aviinfo)
{
	u8* buf;
	buf = (u8*)malloc(100); // 申请100字节内存
    if(NULL == buf){
        printf("[E][videoplayed.cpp] video_info_show(): malloc failed to apply for memory\r\n");
        return;
    }

	sprintf((char*)buf, "声道数: %hu, 采样率:%lu", aviinfo->Channels, aviinfo->SampleRate);
	printf("%s\r\n", buf);
	sprintf((char*)buf, "帧率: %lu", 1000/(aviinfo->SecPerFrame/1000));  // 1000ms/(us/1000=ms)
	printf("%s\r\n", buf);
    
    free(buf); 
}

/* 作用: 计算视频总时长
 * 参数: avi信息
 * 返回值: 视频总时长, 单位s
*/
u32 video_cal_time(AVI_INFO* aviinfo)
{
    u32 tmp = 0;
    u32 time_length = 0;
    time_length = (aviinfo->SecPerFrame/1000)*(aviinfo->TotalFrame);  // ms
    tmp = time_length % 1000;
    time_length = time_length / 1000;

    if(tmp > 0){
        time_length++;
    }
    return time_length;
}

/* 作用: LCD显示视频总时长
 * 参数: 视频时长, 单位s
 * 返回值: 0为成功; -1为失败
*/
char lcd_display_videotime(u32 time)
{
    u32 tmp1, tmp2;
    tmp1 = time;
    tmp2 = time / 60;

    if( tmp2 > 9 ){
        printf("[E][videoplayer.cpp] lcd_display_videotime(): Exceeded the display range, unable to display normally\r\n");
        return -1;
    }

    xSemaphoreTake(mutex_v, portMAX_DELAY);  // 申请互斥锁，若没有资源，则无限等待资源

    // 清理显示区域
    tft.fillRect(138, 154, 10, 20, TFT_BLACK); // 画矩形（填充）
    tft.fillRect(158, 154, 10, 20, TFT_BLACK); // 画矩形（填充）
    tft.fillRect(170, 154, 10, 20, TFT_BLACK); // 画矩形（填充）

    tft.drawBitmap(138, 154, number_index[tmp2], 16, 20, TFT_WHITE);        // 显示分钟
    tmp1 = tmp1 % 60;

    tft.drawBitmap(158, 154, number_index[tmp1/10], 16, 20, TFT_WHITE);     // 显示秒数的十位
    tmp1 = tmp1 % 10;

    tft.drawBitmap(170, 154, number_index[tmp1], 16, 20, TFT_WHITE);        // 显示秒数的个位

    xSemaphoreGive(mutex_v);  // 释放互斥锁

    return 0;
}

/* 作用: 得到stream流信息
 * 参数: 流开始地址(必须是01wb/00wb/01dc/00dc开头)
 * 返回值: AVI_OK为成功; AVI_STREAM_ERR为失败
*/
AVISTATUS avi_get_streaminfo(u8* buf)
{
	avix.StreamID = Loc_MAKEWORD(buf + 2);      // 得到流类型
	avix.StreamSize = Loc_MAKEDWORD(buf + 4);   // 得到流大小

	if (avix.StreamSize % 2) avix.StreamSize++; // 奇数加1（avix.StreamSize 必须是偶数）
	if (avix.StreamID == AVI_VIDS_FLAG || avix.StreamID == AVI_AUDS_FLAG) return AVI_OK;
	return AVI_STREAM_ERR;
}

/* 作用: 定时完成后，调用该函数（每次触发时都会调用回调函数）
 * 参数: 传入状态值
 * 返回值: 无
*/
void TimerRoutine(int state)
{
    frameup = 1;
}