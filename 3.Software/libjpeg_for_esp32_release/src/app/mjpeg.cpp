#include "mjpeg.h"

#define boolean libjpeg_boolean
#include "../jpeg-9e/jpeglib.h"
#undef boolean

struct jpeg_decompress_struct *cinfo;
struct my_error_mgr *jerr;

int Windows_Width = 0;
int Windows_Height = 0;

typedef struct my_error_mgr* my_error_ptr;

METHODDEF(void) my_error_exit(j_common_ptr cinfo)
{
    my_error_ptr myerr = (my_error_ptr)cinfo->err;
    (*cinfo->err->output_message) (cinfo);
    longjmp(myerr->setjmp_buffer, 1);
}

METHODDEF(void) my_emit_message(j_common_ptr cinfo, int msg_level)
{
    my_error_ptr myerr = (my_error_ptr)cinfo->err;
    if (msg_level < 0)
    {
        printf("emit msg:%d\r\n", msg_level);
        longjmp(myerr->setjmp_buffer, 1);
    }
}

/* 作用: 解码一副JPEG图片
 * 参数: 
 *       buf: jpeg数据流数组
 *       bsize: 数组大小
 * 返回值: 0,成功; 其它为错误
*/      
u8 mjpegdec_decode(u8* buf, u32 bsize)
{
    JSAMPARRAY buffer;        /* Output row buffer */ // unsigned char ** buffer;
    if (bsize == 0) return 1;
    int row_stride;		      /* physical row width in output buffer */

    cinfo->err = jpeg_std_error(&jerr->pub);
    jerr->pub.error_exit = my_error_exit;
    jerr->pub.emit_message = my_emit_message;
    cinfo->out_color_space = JCS_RGB;

    if (setjmp(jerr->setjmp_buffer)) // 错误处理
    {
        jpeg_abort_decompress(cinfo);
        jpeg_destroy_decompress(cinfo);
        return 2;
    }

    jpeg_create_decompress(cinfo);

    jpeg_mem_src(cinfo, buf, bsize);   // 测试正常
    jpeg_read_header(cinfo, TRUE);

    jpeg_start_decompress(cinfo); 

#if __DEBUG__
    printf("image_width = %d\n", cinfo->image_width);
    printf("image_height = %d\n", cinfo->image_height);
    printf("num_components = %d\n", cinfo->num_components);

    printf("output_width = %d\n", cinfo->output_width);
    printf("output_components = %d\n", cinfo->output_components);
#endif

    row_stride = cinfo->output_width * cinfo->output_components;

    // 计算buffer大小并申请相应空间
    buffer = (*cinfo->mem->alloc_sarray)
        ((j_common_ptr)cinfo, JPOOL_IMAGE, row_stride, 1);

    int j = 0;       // 记录当前解码的行数
    int lineR = 0;   // 每一行R分量的起始位置

    unsigned char *LCD_Buffer = (unsigned char *)malloc(240*135*2);
    if(NULL == LCD_Buffer){
        printf("[E][mjpeg.cpp] mjpeg_decode(): malloc failed to apply for memory\r\n");
        return 3;
    }

    while (cinfo->output_scanline < cinfo->output_height)
    {
       int i = 0;
        
       jpeg_read_scanlines(cinfo, buffer, 1);
       unsigned short tmp_color565;

        // 为上述图像数据赋值
        for (int k = 0; k < Windows_Width*2; k += 2)
        {
            tmp_color565 = tft.color565(buffer[0][i],buffer[0][i + 1],buffer[0][i + 2]);
            LCD_Buffer[lineR + k] = (tmp_color565 & 0xFF00) >> 8;
            LCD_Buffer[lineR + k + 1] =  tmp_color565 & 0x00FF;

            i += 3;
        }
        
        j++;
        lineR = j * Windows_Width * 2;    
    }

    // 执行显示操作
    xSemaphoreTake(mutex_v, portMAX_DELAY);  // 申请互斥锁，若没有资源，则无限等待资源
    tft.pushImage(0, 0,  Windows_Width, Windows_Height, (unsigned short *)LCD_Buffer);
    xSemaphoreGive(mutex_v);  // 释放互斥锁


    free(LCD_Buffer);
    jpeg_finish_decompress(cinfo);
    jpeg_destroy_decompress(cinfo);
    return 0;
}


/* 作用: mjpeg 解码初始化
 * 参数: offx,offy:x,y方向的偏移
 * 返回值: 0,成功; 1,失败
*/       
char mjpegdec_init(u16 offx, u16 offy)
{
    cinfo = (struct jpeg_decompress_struct *)malloc(sizeof(struct jpeg_decompress_struct));
    jerr = (struct my_error_mgr *)malloc(sizeof(struct my_error_mgr));

    if (cinfo == NULL || jerr == NULL)
    {
        printf("[E][mjpeg.cpp] mjpegdec_init(): malloc failed to apply for memory\r\n");
        mjpegdec_free();
        return -1;
    }
    return 0;
}

/* 作用: mjpeg结束,释放内存
 * 参数: 无
 * 返回值: 无
*/
void mjpegdec_free(void)
{
    free(cinfo);
    free(jerr);
}