#include "read_ziku.h"


/* 作用: 根据字符(utf8编码)获得字模数据
 * 参数: 
 *       para: 字符存储首地址
 *       zimo_tmp: 获得字模数据的存储地址
 * 返回值: 0为获取成功，-1为获取失败
*/
char read_single_ziku(char *para, unsigned char *zihu_tmp)
{
    File file;
    fs::FS &fs = *(fs::FS *)&SD_MMC;
    long long ziku_offset = 0;

	// UTF-8 转换为 unicode 偏移量
    ziku_offset = utf8_to_unicode(para);
    ziku_offset *= SINGLE_ZIKU_SIZE;

    file = fs.open(ZIKU_FILE_PATH);
    if (file) {
#if __DEBUG__
		printf("[M][read_ziku.cpp] read_single_ziku(): open ziku file ok\r\n");
#endif
        file.seek(ziku_offset);
        file.read(zihu_tmp, SINGLE_ZIKU_SIZE);  // 读取 60 字节数据
		file.close();
    	return 0;
     }
     else{
		printf("[E][read_ziku.cpp] read_single_ziku(): open ziku file error\r\n");
		file.close();
        return -1;
     }
}

/* 作用: 根据 UTF-8 编码值转换为 Unicode 字符集偏移量
 * 参数: 编码值首地址
 * 返回值: Unicode 字符集偏移量 
*/
int utf8_to_unicode(char *parm)
{
	int res = 0;
	if((parm[0] & 0x80) == 0x00){
		res = parm[0] & 0x7F;
	}
	else if((parm[0] & 0xE0) == 0xC0){
		res = parm[1] & 0x3F;
		res |= ((parm[0] & 0x1F) << 6);
	}
	else if((parm[0] & 0xF0) == 0xE0){
		res = parm[2] & 0x3F;
		res |= ((parm[1] & 0x3F) << 6);
		res |= ((parm[0] & 0x0F) << 12);
	}
	else if((parm[0] & 0xF8) == 0xF0){
		res = parm[3] & 0x3F;
		res |= ((parm[2] & 0x3F) << 6);
		res |= ((parm[1] & 0x0F) << 12);
		res |= ((parm[0] & 0x7)  << 18);
	}
	else{
		res = -1;
	}

	return res;
}