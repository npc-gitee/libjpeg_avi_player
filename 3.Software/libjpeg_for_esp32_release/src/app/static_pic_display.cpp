#include "static_pic_display.h"

/* 作用: 读取pic图片显示数据
 * 参数: 保存读取的显示数据首地址
 * 返回值: 0, 读取成功; -1, 读取失败
*/
int read_love_pic(unsigned char *image_tmp)
{
    unsigned int file_size = 0;
    File file;
    fs::FS &fs = *(fs::FS *)&SD_MMC;

    file = fs.open(IMAGE_LOVE_FILE_PAYH);
    if (file) {
        file_size = file.size();
#if __DEBUG__
        printf("[M][static_pic_display.cpp] read_love_pic(): open love image file ok\r\n");
        printf("[M][static_pic_display.cpp] read_love_pic(): file_size-%d\r\n", file_size);
#endif
        file.read(image_tmp, file_size);  // 读取图像全部数据
     }
     else{
        printf("[E][static_pic_display.cpp] read_love_pic(): open love image file error\r\n");
        return -1;
     }

    file.close();
    return 0;
}

/* 作用: 显示pic图片
 * 参数: 无
 * 返回值: 无
*/
void lcd_display_love(void)
{
    void * image_ptr = NULL;
    int res = 0;

    image_ptr = malloc(1500);
    if(NULL == image_ptr){
        printf("[E][static_pic_display.cpp] lcd_display_love(): Malloc failed to apply for memory\r\n");
        return;
    }

    res = read_love_pic((unsigned char *)image_ptr);
    if(-1 == res){
        free(image_ptr);
        return;
    }

    tft.pushImage(12, 152, IMAGE_LOVE_WIDTH, IMAGE_LOVE_HEIGHT, (uint16_t *)image_ptr);
    free(image_ptr);
}

/* 作用: 读取pic图片显示数据
 * 参数: 保存读取的显示数据首地址
 * 返回值: 0, 读取成功; -1, 读取失败
*/
int read_pig_pic(unsigned char *image_tmp)
{
    unsigned int file_size = 0;
    File file;
    fs::FS &fs = *(fs::FS *)&SD_MMC;

    file = fs.open(IMAGE_PIG_FILE_PAYH);
    if (file) {
        file_size = file.size();  
#if __DEBUG__
        printf("[M][static_pic_display.cpp] read_pig_pic(): open pig image file ok\r\n");
        printf("[M][static_pic_display.cpp] read_pig_pic():file_size-%d\r\n", file_size);
#endif
        file.read(image_tmp, file_size);  // 读取图像全部数据
     }
     else{
        printf("[E][static_pic_display.cpp] read_pig_pic(): open pig image file error\r\n");
        return -1;
     }

    file.close();
    return 0;
}

/* 作用: 显示pic图片
 * 参数: 无
 * 返回值: 无
*/
void lcd_display_pig(void)
{
    void * image_ptr = NULL;
    int res = 0;

    image_ptr = malloc(1500);
    if(NULL == image_ptr){
        printf("[E][static_pic_display.cpp] lcd_display_love(): Malloc failed to apply for memory\r\n");
        return;
    }

    res = read_pig_pic((unsigned char *)image_ptr);
    if(-1 == res){
        free(image_ptr);
        return;
    }

    tft.pushImage(12, 199, IMAGE_PIG_WIDTH, IMAGE_PIG_HEIGHT, (uint16_t *)image_ptr);
    free(image_ptr);
}