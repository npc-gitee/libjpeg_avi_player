#include "sound_out.h"

static int pcm16_to_pcm8_to_dac16(short *buffer_pcm16, char *buffer_dac16, int count);

/* 作用: I2S初始化
 * 参数: 
 *       sampleRate: 采样率
 *       bitpersample: 位深
 * 返回值: 无
*/
void csound_audioInit(int sampleRate, int bitpersample)
 {
     i2s_config_t i2s_config = {
      .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX  | I2S_MODE_DAC_BUILT_IN),
      .sample_rate = sampleRate,
      .bits_per_sample = (i2s_bits_per_sample_t)bitpersample,
      .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT, //双通道
      .communication_format = I2S_COMM_FORMAT_I2S_MSB,
      .intr_alloc_flags = 0,    // default interrupt priority
      .dma_buf_count = 8,       // 这参数调试过，效果相对较好
      .dma_buf_len = 256,       // 这参数调试过，效果相对较好
      .use_apll = false
     };
 
     i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL); //install and start i2s driver
     i2s_set_dac_mode(I2S_DAC_CHANNEL_RIGHT_EN); //可以控制有几个声道出声音
 }
 
 /* 作用: I2S卸载
  * 参数: 无
  * 返回值: 无
*/
void csound_audioDeinit(void)
 {
      i2s_set_dac_mode(I2S_DAC_CHANNEL_DISABLE);
      i2s_driver_uninstall(I2S_NUM_0);
 }

/* 作用: 播放WAV音频
 * 参数: 
 *       buffer: 有符号16位pcm数据(一帧音频帧)存储的首地址
 *       frame_size: 一帧音频帧字节数
 * 返回值: 无
*/
void play_wav(uint8_t *buffer, uint32_t frame_size)
{
    int wr_len = 0;
    size_t bytes_written;
    
    char *i2s_wr_buffer = (char *)malloc(FRAME_SIZE);
    if(NULL == i2s_wr_buffer){
      printf("[E][sound_out.cpp] play_wav(): malloc failed to apply for memory\n");
      return;
    }
    
    wr_len = pcm16_to_pcm8_to_dac16((short *)buffer, i2s_wr_buffer, frame_size);
    
    //音频数据，将通过I2S的方式发送给内部DAC
    i2s_write(I2S_NUM_0, i2s_wr_buffer, wr_len, &bytes_written, 100);
    free(i2s_wr_buffer);
}


/* 作用: 将有符号PCM16数据转换为无符号PCM8数据，为了适配DAC，将PCM8位数据转换为16位
 * 参数: 
 *      buffer_pcm16: 解码完成的数据
 *      buffer_dac16: 用于I2S发送给DAC的数据
 *      count: pcm16 数据量,单位为字节
 * 返回值: 音频数据量，单位为字节
*/
static int pcm16_to_pcm8_to_dac16(short *buffer_pcm16, char *buffer_dac16, int count)
{
  short tmp16 = 0;
  char tmp8 = 0;
  uint8_t utmp8 = 0;
  uint32_t j = 0;
  
  count /= 2;  // buffer_pcm16指针按双字节读取，所以这里需要先除以2，不然就越界了

  for(int i = 0; i <= count; i++){
    tmp16 = *(buffer_pcm16 + i);
    tmp8 = tmp16 >> 8;
    utmp8 = tmp8 + 128;
    buffer_dac16[j++] = 0;      //I2S ADC的规则,所以将8位PCM转换成16位数据
    buffer_dac16[j++] = utmp8;
  }
  return count*2;
}
