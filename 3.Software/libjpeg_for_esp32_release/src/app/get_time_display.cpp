#include "get_time_display.h"

const char utf8_num[24] = "日一二三四五六";

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org"); // NTP 服务器地址
int pre_day = 0;


/* 作用: Wifi 初始化
 * 参数: 无
 * 返回值: 无
*/
void get_wifi(void)
{
    char *ssid = (char *)malloc(64);
    if(NULL == ssid){
      printf("[E][get_time_display.cpp] get_wifi(): malloc failed to apply for memory [1]\r\n");
      return;
    }

    char *password = (char *)malloc(64);
    if(NULL == password){
      printf("[E][get_time_display.cpp] get_wifi(): malloc failed to apply for memory [2]\r\n");
      free(ssid);
      return;
    }

    // 获取ssid和password
    get_ssid_password(ssid, password);

    // 连接网络
    WiFi.begin(ssid, password);

    free(ssid);
    free(password);

    // 等待网络连接成功
    while ( WiFi.status() != WL_CONNECTED ) {
      delay ( 500 );
      Serial.print ( "." );
    }

    Serial.println("");
    Serial.println("WiFi connected");  // 连接成功
    Serial.print("IP address: ");      // 打印IP地址
    Serial.println(WiFi.localIP());
}

/* 作用: 通过PC端串口输入获取Wifi名称和密码
 * 参数: 
 *       ssid_buf: 用于存储wifi名称首地址
 *       password: 用于存储wifi密码首地址
 * 返回值: 无 
*/
void get_ssid_password(char *ssid_buf, char *password_buf)
{
    int i = 0;
    Serial.println("[plz input ssid]>");
    while(1){
      if(Serial.available()){
        ssid_buf[i] = Serial.read();
        if(ssid_buf[i] == '\n'){
          break;
        }
        else{
          i++;
        }
      }
    }
    ssid_buf[i] = '\0';
    i = 0;

    Serial.println("[plz input password]>");
    while(1){
      if(Serial.available()){
        password_buf[i] = Serial.read();
        if(password_buf[i] == '\n'){
          break;
        }
        else{
          i++;
        }
      }
    }
    password_buf[i] = '\0';
}

/* 作用: 通过NTPClient获取时间
 * 参数: 这里无功能，为了符合xTaskCreate函数参数类型
 * 返回值: 无
*/
void get_time_display(void *para)
{
    int year, current_day, month, weekday;
    struct tm *ptm;
    
    timeClient.begin();
    timeClient.setTimeOffset(28800);  // 切换到东八区

    while(1){
        timeClient.update(); // 更新时间
        unsigned long epochTime = timeClient.getEpochTime();

        weekday = timeClient.getDay();

        //将epochTime换算成年月日
        ptm = gmtime(( time_t * )&epochTime);

        year = ptm->tm_year+1900;
        year %= 100;                   // 显示区域不够，这里只显示后两位
        month = ptm->tm_mon+1;
        current_day = ptm->tm_mday;

        if(pre_day != current_day){
            // 时间更新则显示, 不变则不刷新显示
            xSemaphoreTake(mutex_v, portMAX_DELAY);  // 申请互斥锁，若没有资源，则无限等待资源
            display_time(year, month, current_day, weekday);
            xSemaphoreGive(mutex_v);  // 释放互斥锁
            pre_day= current_day;
        }
        
        vTaskDelay(1000);  // 1s刷新一次
#if __DEBUG__
        printf("[M][get_time_display.cpp] get_time_display(): %d-%d-%d %d\r\n", year, month, current_day, weekday);
#endif
    }

    
}

/* 作用: LCD 显示时间操作
 * 参数: 
 *       year: 年(2023年,后两位，即23)
 *       month: 月
 *       day: 日
 *       weekday: 星期
 * 返回值: 无
*/
void display_time(int year, int month, int day, int weekday)
{
    unsigned char *zimo_buffer;

    tft.fillRect(43, 203, 28, 20, TFT_BLACK); // 清屏
    // 显示年      
    tft.drawBitmap(43, 203, number_index[year/10], 16, 20, TFT_WHITE);    // "2"
    tft.drawBitmap(55, 203, number_index[year%10], 16, 20, TFT_WHITE);    // "3"

    tft.fillRect(91, 203, 28, 20, TFT_BLACK); // 清屏
    // 显示月
    tft.drawBitmap(91, 203, number_index[month/10], 16, 20, TFT_ORANGE);   // "1->12"
    tft.drawBitmap(103, 203, number_index[month%10], 16, 20, TFT_ORANGE);

    tft.fillRect(135, 203, 28, 20, TFT_BLACK); // 清屏
    // 显示日
    tft.drawBitmap(135, 203, number_index[day/10], 16, 20, TFT_ORANGE);    // "1->31"
    tft.drawBitmap(147, 203, number_index[day%10], 16, 20, TFT_ORANGE);

    tft.fillRect(205, 203, 24, 20, TFT_BLACK); // 清屏
    // 显示星期
    zimo_buffer = (unsigned char *)malloc(128);
    if(NULL == zimo_buffer){
        printf("[E][get_time_display.cpp] diaplaytime(): malloc failed to apply for memory\r\n");
        return;
    }
    read_single_ziku((char *)utf8_num+3*weekday, zimo_buffer);             // "日一二三四五六"
    tft.drawBitmap(205, 203, zimo_buffer, 24, 20, TFT_WHITE);

    free(zimo_buffer);
}
