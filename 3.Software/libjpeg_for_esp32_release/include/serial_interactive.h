#ifndef __SERIAL_INTERACTIVE__
#define __SERIAL_INTERACTIVE__

#include "HardwareSerial.h"
#include "FS.h"
#include "SD.h"
#include "SD_MMC.h"
#include "videoplayer.h"
#include "read_ziku.h"

extern struct videoplayer_parm global_videoplayer_parm;

void help_info(void);
void mySerial_Recv(char *buffer);
void commd_ls(fs::FS &fs, const char * dirname, uint8_t levels);
void commd_play(fs::FS &fs, char *buf);
char is_avi_file(char *buf, int str_len);
void EtoC_index(fs::FS &fs, char *buf);
int number_of_words(char *buf);

#endif