#ifndef __MY_COMMON__
#define __MY_COMMON__


#define __DEBUG__ 0

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef volatile unsigned char vu8;
typedef volatile unsigned long vu32;

#endif