#ifndef __READZIKU__
#define __READZIKU__


#include "FS.h"
#include "SD.h"
#include "SD_MMC.h"
#include "common.h"


#define ZIKU_FILE_PATH "/ziku20x20_bold"
#define SINGLE_ZIKU_SIZE 60  // 一个符号字模大小60字节


int utf8_to_unicode(char *para);
char read_single_ziku(char *parm, unsigned char *zihu_tmp);


#endif