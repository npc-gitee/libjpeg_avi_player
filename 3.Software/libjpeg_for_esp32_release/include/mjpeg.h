#ifndef __MJPEG__
#define __MJPEG__

#include <SPI.h>
#include "TFT_eSPI.h"
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include "common.h"

#define boolean libjpeg_boolean
#include "../jpeg-9e/jpeglib.h"
#undef boolean


struct my_error_mgr {
    struct jpeg_error_mgr pub;	/* "public" fields */
    jmp_buf setjmp_buffer;	    /* for return to caller */
};


// 变量声明
extern TFT_eSPI tft;
extern int Windows_Width;       // 图像宽度
extern int Windows_Height;      // 图像高度
extern unsigned char* r;
extern SemaphoreHandle_t mutex_v;


// 函数声明
char mjpegdec_init(u16 offx, u16 offy);
void mjpegdec_free(void);
u8 mjpegdec_decode(u8* buf, u32 bsize);

#endif