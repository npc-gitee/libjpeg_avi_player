#ifndef __SOUND_OUT__
#define __SOUND_OUT__

#include <Arduino.h>
#include <driver/i2s.h>

//#define FRAME_SIZE 1024*5
#define FRAME_SIZE 5000

void csound_audioInit(int sampleRate, int bitpersample);
void csound_audioDeinit(void);
void play_wav(uint8_t *buffer, uint32_t frame_size);


#endif