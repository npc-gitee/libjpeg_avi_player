#ifndef __VEDIOPLAYER__
#define __VEDIOPLAYER__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mjpeg.h"
#include "avi.h"
#include "common.h"
#include <Ticker.h>
#include "sound_out.h"

#include "FS.h"
#include "SD.h"
#include "SD_MMC.h"
#include "number_zimo_data.h"
#include "TFT_eSPI.h"
#include "music_dance.h"

extern TFT_eSPI tft;
extern SemaphoreHandle_t mutex_v;

struct videoplayer_parm
{
    void * sdmmc;
    const char* file_name;
};

//#define AVI_AUDIO_BUF_SIZE    1024*5		//定义avi解码时,音频buf大小.
#define AVI_AUDIO_BUF_SIZE    5000		    //定义avi解码时,音频buf大小.
#define AVI_FILE_HEAD_SIZE    1024*60
#define AVI_VIDEO_BUF_SIZE    1024*20		//定义avi解码时,视频buf大小.

#define	 Loc_MAKEWORD(ptr)	(u16)(((u16)*((u8*)(ptr))<<8)|(u16)*(u8*)((ptr)+1))
#define  Loc_MAKEDWORD(ptr)	(u32)(((u16)*(u8*)(ptr)|(((u16)*(u8*)(ptr+1))<<8)|(((u16)*(u8*)(ptr+2))<<16)|(((u16)*(u8*)(ptr+3))<<24))) 

extern vu32 frameup;

void video_play_mjpeg(void *parm);
void video_info_show(AVI_INFO* aviinfo);
AVISTATUS avi_get_streaminfo(u8* buf);
void TimerRoutine(int state);
u32 video_cal_time(AVI_INFO* aviinfo);
char lcd_display_videotime(u32 time);

#endif