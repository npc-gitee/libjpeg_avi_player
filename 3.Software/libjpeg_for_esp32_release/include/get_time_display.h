#ifndef __GET_TIME_DISPLAY__
#define __GET_TIME_DISPLAY___


#include "NTPClient.h"
#include <WiFi.h>
#include <WiFiUdp.h>
#include "TFT_eSPI.h"
#include "number_zimo_data.h"
#include "read_ziku.h"
#include "lcd_layout.h"

extern TFT_eSPI tft;
extern SemaphoreHandle_t mutex_v;

void get_ssid_password(char *ssid_buf, char *password_buf);
void get_wifi(void);
void get_time_display(void *para);
void display_time(int year, int month, int day, int weekday);

#endif