#ifndef __MUSIC_DANCE__
#define __MUSIC_DANCE__


#include "TFT_eSPI.h"
#include "common.h"

extern TFT_eSPI tft;
extern SemaphoreHandle_t mutex_v;  // 互斥量

void music_dance_init(int width);
void music_dance(void *para);

#endif