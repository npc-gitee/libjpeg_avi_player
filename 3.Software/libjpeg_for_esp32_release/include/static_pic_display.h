#ifndef __STATIC_PIC_DISPLAY__
#define __STATIC_PIC_DISPLAY__


#include "FS.h"
#include "SD.h"
#include "SD_MMC.h"
#include "TFT_eSPI.h"
#include "common.h"

#define IMAGE_LOVE_FILE_PAYH "/image_love.bin"
#define IMAGE_PIG_FILE_PAYH "/image_pig.bin"

#define IMAGE_LOVE_WIDTH 27
#define IMAGE_LOVE_HEIGHT 25
#define IMAGE_LOVE_FILE_SIZE 1350

#define IMAGE_PIG_WIDTH 27
#define IMAGE_PIG_HEIGHT 26
#define IMAGE_PIG_FILE_SIZE 1404

extern TFT_eSPI tft;


void lcd_display_love(void);
void lcd_display_pig(void);

#endif