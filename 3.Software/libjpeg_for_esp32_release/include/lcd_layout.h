#ifndef __LCD_LAYOUT__
#define __LCD_LAYOUT__

#include "TFT_eSPI.h"
#include "number_zimo_data.h"
#include "read_ziku.h"
#include <Ticker.h>
#include "common.h"
#include "static_pic_display.h"

extern TFT_eSPI tft;

void lcd_layout_init(void);

#endif